Ссылка на материалы с прошлых школ: : https://yadi.sk/d/phws8YjUkhbIhQ  

Телеграм https://t.me/sbrf_java_school_09_2019  

Ссылка на вступление в класс на Stepik: https://stepik.org/join-class/0f562e75fb2cac57f13bfeaf1e8e049ee38d2f55

Репозиторий с проектом которым занимаемся на практической части: https://gitlab.com/sbrf-java-school-2019-09/practice

Записи лекций:
1) История и основы - https://www.youtube.com/watch?v=m8mO1s07Rro 
2) Коллекции, дженерики и гит - https://www.youtube.com/watch?v=khXYGctNyMM
3) Исключения, Рефлексия, Аннотации, Прокси - https://www.youtube.com/watch?v=bzLRiqT52yU
4) Classloader - https://www.youtube.com/watch?v=eFdltxZraik
5) Maven, логирование - https://www.youtube.com/watch?v=81hXuWx-p4w
6) Unit-test, TestNG, Mockito - https://www.youtube.com/watch?v=LbA7CFqgvzA
7) Serialization - https://www.youtube.com/watch?v=x_GczQNSsjA
8) Java 8 (lambda, Stream API) - https://www.youtube.com/watch?v=LAdDpU8wBuI
9) Multithreading - https://www.youtube.com/watch?v=qCWyeZAygEI
10) Memory Model, java.util.concurrency - https://www.youtube.com/watch?v=Skz98e15UxM
11) Sockets - https://www.youtube.com/watch?v=oVNMbinKfyw
12) JVM, JIT - https://www.youtube.com/watch?v=hUaDUvxYLVA
13) GC - https://www.youtube.com/watch?v=K0G6hK9o_aw

spring https://youtu.be/ajv6Ipxoe30
